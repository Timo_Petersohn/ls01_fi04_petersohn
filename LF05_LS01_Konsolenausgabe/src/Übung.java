
public class Übung {
	public static void main(String[] args) {
	
	String s = "-> Scherbeln Wäre besser <-";
	
	System.out.printf( "|%-30s|\n", s );
	System.out.printf( "|%30s|\n", s );
	System.out.printf( "|%10s|\n", s );
	System.out.printf( "|%.12s|\n", s );
	System.out.printf( "|%30.12s|\n", s );
	System.out.println();
	System.out.printf( "Und jetzt zu die Zahlen\n", s );
	
	int i = 537;
	System.out.printf("|%d|     |%d|\n", i, -i);
	System.out.printf("|%7d|     |%7d|\n", i, -i);
	System.out.printf("|%-7d|     |%-7d|\n", i, -i);
	System.out.printf("|%+-7d|     |%+-7d|\n", i, -i);
	System.out.printf("|%07d|     |%07d|\n", i, -i);
	System.out.printf("|%x|     |%x|\n", i, -i);
}
}