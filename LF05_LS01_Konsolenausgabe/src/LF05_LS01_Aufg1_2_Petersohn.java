public class LF05_LS01_Aufg1_2_Petersohn 	{

	public static void main(String[] args) 		{
		
//		Aufgabe 1: 
			
	System.out.printf("%4s%s%n","*", "*");
	System.out.printf("%-4s%4s%n","*", "*");
	System.out.printf("%-4s%4s%n","*", "*");
	System.out.printf("%4s%s%n","*", "*");
		
	System.out.printf("%s%n","________________________________");
	
//		Aufgabe 2 (Fakultaet):


	System.out.printf("%-5s%s%-19s%s%4s%n","0!","=","","="," 1");
	System.out.printf("%-5s%s%-19s%s%4s%n","1!","="," 1 ","="," 1");
	System.out.printf("%-5s%s%-19s%s%4s%n","2!","="," 1 * 2","="," 2");
	System.out.printf("%-5s%s%-19s%s%4s%n","3!","="," 1 * 2 * 3","="," 6");
	System.out.printf("%-5s%s%-19s%s%4s%n","4!","="," 1 * 2 * 3 * 4","="," 24");
	System.out.printf("%-5s%s%-19s%s%4s%n","5!","="," 1 * 2 * 3 * 4 * 5","="," 120");

	System.out.printf("%s%n","________________________________");
//	Aufgabe 3: Temeraturtabelle
	
	System.out.printf("%-12s|%10s%n","Fahrenheit", "Celsius");
	System.out.printf("%s%n","-----------------------");
	System.out.printf("%-12s|%10s%n", -20, -28.8889);
	System.out.printf("%-12s|%10s%n", -10, -23.3333);
	System.out.printf("%-12s|%10s%n", +0, -17.7778);
	System.out.printf("%-12s|%10s%n", +20, -6.6667);
	System.out.printf("%-12s|%10s%n", +30, -1.1111);
	
	System.out.printf("%s%n","________________________________");
	}  
} 