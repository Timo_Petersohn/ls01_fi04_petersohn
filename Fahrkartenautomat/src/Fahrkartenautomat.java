﻿import java.util.Scanner;

class Fahrkartenautomat

{
	public static double fahrkartenbestellungErfassen () {
		
		Scanner tastatur = new Scanner (System.in);
		
		int kartenWahl;
		int anzahlFahrkarten;
		double preis;
		double zuZahlenderBetrag;
		String[] fahrkarten = new String[10];
		double[] fahrkartenPreis = new double[10];
		
		//Nachdem die Variablen initiatilsiert wurden beginnt das definieren der einzelnen Array-Elemente
		//Der Vorteil von Arrays ist es, dass eventuelle Änderungen lediglich im Array vorgenommen werden müssen
		//So hat man all seine definierten Fahrkerten mit ihren Preisen, welche im Array einfach angepasst werden können
		
		fahrkarten[0] = "Einzelfahrschein Berlin AB";
		fahrkarten[1] = "Einzelfahrschein Berlin BC";
		fahrkarten[2] = "Einzelfahrschein Berlin ABC";
		fahrkarten[3] = "Kurzstrecke";
		fahrkarten[4] = "Tageskarte Berlin AB";
		fahrkarten[5] = "Tageskarte Berlin BC";
		fahrkarten[6] = "Tageskarte Berlin ABC";
		fahrkarten[7] = "Kleingruppen-Tageskarte Berlin AB";
		fahrkarten[8] = "Kleingruppen-Tageskarte Berlin BC";
		fahrkarten[9] = "Kleingruppen-Tageskarte Berlin ABC";
		
		fahrkartenPreis[0] = 2.90;
		fahrkartenPreis[1] = 3.30;
		fahrkartenPreis[2] = 3.60;
		fahrkartenPreis[3] = 1.90;
		fahrkartenPreis[4] = 8.60;
		fahrkartenPreis[5] = 9.00;
		fahrkartenPreis[6] = 9.60;
		fahrkartenPreis[7] = 23.50;
		fahrkartenPreis[8] = 24.30;
		fahrkartenPreis[9] = 24.90;
		
		System.out.println("Bitte wählen Sie eine Fahrkarte durch Tippen auf die entsprechende Ziffer:\n");
			
			for (int i=0; i < fahrkarten.length; i++) 
			{
				int wahl = i + 1;
	            System.out.println(fahrkarten[i] + " [" + fahrkartenPreis[i] + "0 EUR] ("+ wahl + ")" );
			}
				System.out.print("\n");
				System.out.print("Ihre Wahl:");
				
				
			kartenWahl = tastatur.nextInt();
			
			while (kartenWahl > 10 || kartenWahl < 1) {
		    	System.out.println ("Bitte wählen Sie eine gültige Fahrkarte!");
		    	System.out.println("Ihre Wahl:");
		    	kartenWahl = tastatur.nextInt();
		    }
			
			preis = fahrkartenPreis[kartenWahl - 1];
			
			
	    System.out.print("Bitte Anzahl der Karten eingeben: ");
	    anzahlFahrkarten = tastatur.nextInt();
	    zuZahlenderBetrag = preis * anzahlFahrkarten;
	    
	    if (anzahlFahrkarten<1 || anzahlFahrkarten>10) {	
    		System.out.println("Es wird leider nur eine Anzahl zwischen 1 und 10 akzeptiert. Es wird mit einer einzelnen Fahrkarte weitergerechnet.");
    	anzahlFahrkarten = 1;
	    }
	    	
	    return zuZahlenderBetrag;
	}
	
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		//Geldeinwurf
		
	       System.out.print("Vielen Dank für ihr Vertrauen. ");
	       
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) 
		{
			System.out.printf("%s%.2f%n", "Noch zu zahlen: " ,  (zuZahlenderBetrag - eingezahlterGesamtbetrag) , "€" );
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		
		}
		
	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	return rückgabebetrag;
	}
	
	public static void fahrkartenAusgeben() {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public static void rückgeldAusgeben (double rückgeld) {
	       

	       double rückgabebetrag;
	       rückgabebetrag = rückgeld;
	       
 	   System.out.printf("%s%.2f%s%n" , "Der Rückgabebetrag in Höhe von " , rückgabebetrag , "€");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }
    

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
    	
    			
	}
	

	
    public static void main(String[] args) {
    	double zuZahlen;
    	double rückgeld;		
    	boolean schleife;
    	
    	schleife = true;
    do {	
    	zuZahlen = fahrkartenbestellungErfassen();
    	rückgeld = fahrkartenBezahlen(zuZahlen);
    	fahrkartenAusgeben();
    	rückgeldAusgeben (rückgeld);
    	
    	
    	for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(25);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	      }
    	System.out.println("=");
    	System.out.println("Noch mehr Fahrkarten?");
    	System.out.println("");

    }while (schleife == true);
    }
} 